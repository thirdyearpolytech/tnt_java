package com.teste;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Hello World");

		Group root = new Group();

		Scene scene = new Scene(root, 300, 250, Color.LIGHTGREEN);

		Button btn = new Button();

		btn.setLayoutX(100);

		btn.setLayoutY(80);

		btn.setText("Hello World");

		btn.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent event) {

				System.out.println("Hello World");

			}

		});

		root.getChildren().add(btn);

		primaryStage.setScene(scene);

		primaryStage.show();

		// try {
		// BorderPane root = new BorderPane();
		// Scene scene = new Scene(root,400,400);
		// scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		// primaryStage.setScene(scene);
		// primaryStage.show();
		// } catch(Exception e) {
		// e.printStackTrace();
		// }
	}

	public static void main(String[] args) {
		launch(args);
	}
}

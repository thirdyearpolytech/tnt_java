package com.teste;

import javax.xml.stream.XMLStreamException;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Graphique extends Application {
	
	public TNT tnt ;
	public ListeChaine listCh;

	@Override
	public void start(Stage primaryStage) throws Exception {
		tnt = new TNT();
		try {
			tnt.parser();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		
		
		primaryStage.setTitle("Television Numerique Terrestre (TNT)");
		
		Group root = new Group();
		
		VBox vbox = new VBox();
		
		Text bienvenue = new Text("BIENVENUE A LA TELEVISION NUMERIQUE TERRESTRE DE FRANCE");
		bienvenue.setFont(new Font(20));
		bienvenue.setFill(Color.WHITE);
		bienvenue.setX(50);
		bienvenue.setY(100);
		
        ImageView tntImage = new ImageView(new Image(Graphique.class.getResourceAsStream("tnt.jpeg")));
	    tntImage.setFitHeight(150);
	    tntImage.setTranslateX(225);
	    tntImage.setTranslateY(180);
	    
	    Text consultChaine = new Text("Consulter les chaines de la TNT");
	    consultChaine.setFont(new Font(20));
	    consultChaine.setFill(Color.WHITE);
		consultChaine.setX(220);
		consultChaine.setY(400);
		consultChaine.setCursor(Cursor.HAND);
	    
		
		root.getChildren().addAll(bienvenue, tntImage, consultChaine);
		
		Scene sc = new Scene(root, 800, 600, Color.WHITE);	
		sc.setFill(new LinearGradient(0f, 0f, 0f, 1f, true, CycleMethod.NO_CYCLE,
						new Stop[] { new Stop(0, Color.web("#333333")), new Stop(1, Color.web("#000000")) }));
		
		
		consultChaine.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				//System.out.println("consulter chaine");
				root.getChildren().clear();
				sc.setFill(Color.WHITE);
				listCh = new ListeChaine(tnt);
				root.getChildren().add(listCh.hbox);
			}
		});
		primaryStage.setScene(sc);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}


}

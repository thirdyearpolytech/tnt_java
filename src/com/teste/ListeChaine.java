package com.teste;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import com.tele.*;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class ListeChaine extends Parent {
	
	public GridPane gridPane;
	public TNT tnt ;
	public List<ImageView> listIm;
	public HBox hbox;
	private List<Chaine> listCh;
	private List<RadioButton> listRb;
	private List<String> listJ;
	private GridPane gridDate;
	private ToggleGroup groupe;

	public ListeChaine(TNT tnt) {
		
		this.tnt = tnt;
		listCh = tnt.listCh;
		listIm = new ArrayList<>();
		// l'organisation de la scene
		hbox = new HBox();
		VBox vbox = new VBox();
		
		gridDate = new GridPane(); // gridpane pour les dates
		Button butAcc = new Button("Accueil");
		
		groupe = new ToggleGroup();

		afficheDate();
		
		listRb.get(0).setFocusTraversable(true); //date selectionnée par défaut
		
		gridDate.setHgap(20);
		gridDate.setVgap(10);
		
		vbox.getChildren().addAll(butAcc, gridDate);
		vbox.setSpacing(10);
		
		// Creating a Grid Pane pour les icones des chaines
		gridPane = new GridPane();
		// Setting size for the pane
		gridPane.setMinSize(600, 600);
		// Setting the padding
		//gridPane.setPadding(new Insets(10, 10, 10, 10));
		// Setting the vertical and horizontal gaps between the columns
		gridPane.setVgap(3);
		gridPane.setHgap(3);
		// Setting the Grid alignment
		gridPane.setAlignment(Pos.TOP_LEFT);
		
		chargerImage();
		
		//tnt.afficherListChaine();
	    // gestion des evenements des chaines
	    for(int i=0; i<listIm.size(); i++) {
	    	ImageView im = listIm.get(i);
	    	im.setCursor(Cursor.HAND);
	    	
	    	im.setOnMouseClicked(new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					System.out.println("ici");
				}
			});
	    }
	    
	    hbox.getChildren().addAll(gridPane, vbox);
	    hbox.setTranslateX(10);
	    hbox.setTranslateY(20);
	}
	
	public void chargerImage() {
		int c = 0;
	    for(int i=0; i<listCh.size(); i++) {
	    	
	    	String url = listCh.get(i).getIcon();
	    	ImageView image = new ImageView(new Image(url));
	    	listIm.add(image);
	    	image.setFitWidth(110);
	        image.setFitHeight(90);
	        //Rectangle2D rect = new Rectangle2D(image.getX(), image.getY(), image.getFitWidth(), image.getFitHeight());
	    
	        //image.viewportProperty();
	        gridPane.add(image, i%5, c);
	        if((i+1)%5 == 0)
	        	c++;
	    }
	}
	
	public void afficheDate() {
		listRb = new ArrayList<>();
		listJ = tnt.jourDemission(); // liste des jours disposant un programme télé
		for(int i=listJ.size()-1; i>=0; i--) {
			Text date = new Text(listJ.get(i));
			RadioButton rbDate  = new RadioButton();
			rbDate.setToggleGroup(groupe);
			listRb.add(rbDate);
			gridDate.add(date, 1, listJ.size()-1-i);
			gridDate.add(rbDate, 0, listJ.size()-1-i);
		}
	}

}

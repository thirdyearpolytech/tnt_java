package com.teste;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import com.personne.*;
import com.tele.Chaine;
import com.tele.Emission;
import com.tele.HashMultiSet;
import com.tele.MultiSet;
// Dans film ya que des guest, donc il faur considerer guest comme actor

public class TNT {
	
	 int i =0; // pour tester le nombre d'emission en rediffusion
	
	 Map<String, List<Emission>> chaineEm = new HashMap<>(); // associer chaque ID de chaine à ses emissions
	 Map<String, String> chaineId = new HashMap<>(); // trouver l'id d'une chaine à partir de son nom
	 Map<String, List<Emission>> nomEm = new HashMap<>();// permet de trouver rapidement les info d'une émission à
																// partir de son nom
	 Map<String, List<Emission>> actFilm = new HashMap<>(); // liste des films d'un acteur
	 Map<String, List<Emission>> realFilm = new HashMap<>();//liste des films d'un réalisateur
	 MultiSet<String> actNbFilm = new HashMultiSet<>(); // les acteurs avec leur nombre de film
	 MultiSet<String> nbTypeEm = new HashMultiSet<>(); // compter le nombre de type d'emission de chaque type
	 MultiSet<String> nbEmCat = new HashMultiSet<>(); // compter le nombre d'emission par categorie par chaine
	 Map<String, Set<Integer>> chDateFilm = new HashMap<>();// chaque chaine la liste des date de diffusion de film
	 MultiSet<String> chDateMoy = new HashMultiSet<>(); // chaque chaine avec la moyenne de diffusion de film
	
	 List<Chaine> listCh = new ArrayList<>();

	 List<String> listEmission = new ArrayList<>();

	 List<Personne> listActeur = new ArrayList<>();
	 List<Personne> listReal = new ArrayList<>();
	
	 Set<String> listCategorie = new TreeSet<>(); // pour tester
	
	public TNT() {
		super();
	}

	public  void parser() throws XMLStreamException {
		XMLInputFactory xmlif = XMLInputFactory.newInstance();
		XMLStreamReader xmlsr = null;
		try {
			xmlsr = xmlif.createXMLStreamReader(new FileReader("tvguide_20180430_20180511.xml"));
		} catch (FileNotFoundException | XMLStreamException e) {
			e.printStackTrace();
		}

		int eventType, i = 0;
		Emission prog = null;

		// sauter la première ligne
		if (xmlsr.hasNext())
			eventType = xmlsr.next();
		if (xmlsr.hasNext())
			eventType = xmlsr.next();

		// i<29 pour les chaine
		while (xmlsr.hasNext()) {
			eventType = xmlsr.next();
			i++;
			switch (eventType) {
			case XMLEvent.START_ELEMENT:
				// traitement pour les chaines
				if (xmlsr.getLocalName().equals("channel")) {
					Chaine ch = construitChaine(xmlsr);
					listCh.add(ch);
					// System.out.println(ch);
					chaineId.put(ch.getNom(), ch.getId());
				} else if (xmlsr.getLocalName().equals("programme")) {
					// eventType = xmlsr.next();
					// System.out.println(xmlsr.getText());
					// listSet.add(xmlsr.getText());
					prog = construireEmission(xmlsr);
					addEmission(chaineEm, prog.getIdChaine(), prog);
					// System.out.println(prog);
				}
				break;
			}
		}
		// associer chaque chaine  par sa date moyenne de diffusion de film
		Iterator<Entry<String, Set<Integer>>> it = chDateFilm.entrySet().iterator();
		Set<Integer> listDate;
		int moy , som = 0;
		while (it.hasNext()) {
			som = 0;
			Entry<String, Set<Integer>> e = it.next();
			listDate = e.getValue();
			for(int d : listDate) {
				som += d;
			}
			// calcul de la moyenne pour chaque chaine
			moy = som/listDate.size();
			chDateMoy.add(e.getKey(), moy);
		}
		
		if (xmlsr != null) {
			xmlsr.close();
		}
	}

	public  Chaine construitChaine(XMLStreamReader xmlsr) throws XMLStreamException {
		String nom = null;
		String icon = null;
		boolean stop = false;
		String balise = xmlsr.getLocalName();
		String id = xmlsr.getAttributeValue(0);
		int evenType = xmlsr.next(); // <display-name>
		// tant la balise fermante n'est pas atteinte
		while (!stop) {
			switch (evenType) {
			// recuperer le nom de la chaine
			case XMLEvent.CHARACTERS:
				nom = xmlsr.getText();
				break;
			// récuperer l'icone
			case XMLEvent.START_ELEMENT:
				if (xmlsr.getLocalName().equals("icon")) {
					icon = xmlsr.getAttributeValue(0);
				}
				break;
			case XMLEvent.END_ELEMENT:
				if (balise.equals(xmlsr.getLocalName())) {
					stop = true;
				}
				break;
			}

			evenType = xmlsr.next();
		}

		return new Chaine(nom, id, icon);
	}

	public  Emission construireEmission(XMLStreamReader xmlsr) throws XMLStreamException {
		String balise = xmlsr.getLocalName();

		String debut = xmlsr.getAttributeValue(0);
		String fin = xmlsr.getAttributeValue(1);
		String idChaine = xmlsr.getAttributeValue(3);
		int duree = 0;
		String titre = null;
		String sousTitre = null;
		String desc = null;
		String continent = null;
		String categorie = null;
		String date = null;
		String type = null;
		boolean stop = false;

		Emission emission = null;
		listActeur = new ArrayList<Personne>();
		listReal = new ArrayList<Personne>();
		int evenType = ignoreEspace(xmlsr);
		
		// tant la balise fermante n'est pas atteinte
		while (!stop) {
			switch (evenType) {
			case XMLEvent.START_ELEMENT:
				switch (xmlsr.getLocalName()) {
				case "title":
					evenType = xmlsr.next();
					titre = xmlsr.getText();
					break;
				case "sub-title":
					evenType = xmlsr.next();
					sousTitre = xmlsr.getText();
					break;
				case "desc":
					evenType = xmlsr.next();
					desc = xmlsr.getText();
					break;
				case "value":
					evenType = xmlsr.next();
					categorie = xmlsr.getText();
					listCategorie.add(categorie);
					break;
				case "length":
					evenType = xmlsr.next();
					duree = new Integer(xmlsr.getText()).intValue();
					break;
				case "country":
					evenType = xmlsr.next();
					continent = xmlsr.getText();
					break;
				case "date":
					evenType = xmlsr.next();
					date = xmlsr.getText();
					break;
				case "actor":
					listActeur.add(constuireActeur(xmlsr));
					break;
				case "director":
					listReal.add(constuireRealisateur(xmlsr));
					break;
				case "category":
					evenType = xmlsr.next();
					type = xmlsr.getText();
					listEmission.add(type.split(" ")[0]);
					break;
				case "previously-shown":
					//System.out.println("rediffusion");
					i++;
					break;
				}
				break;
			case XMLEvent.END_ELEMENT:
				if (balise.equals(xmlsr.getLocalName())) {
					stop = true;
				}
				break;
			}

			evenType = ignoreEspace(xmlsr);
		}
		
		emission = new Emission(type, debut, fin, duree, titre, sousTitre, desc, continent, categorie, idChaine, date);

		//ajouter le film à la liste de l'acteur correspondant
		 for(Personne p : listActeur) {
			 String key = p.getNom() + " " + p.getPrenom();
			 addEmission(actFilm, key, emission);
			 actNbFilm.add(key, 1);//incrémenter le nombre de film de cet acteur
		 }
		 //
		 if(type.split(" ")[0].equals("film")) {
			 String ch = trouverNomEmission(idChaine);
			 ajouterDateFilm(chDateFilm, ch, date);
		 }
		//ajouter le film à la liste du réalisateur correspondant
		 for(Personne p : listReal) {
			 addEmission(realFilm, p.getNom() + " " + p.getPrenom(), emission);
		 }
		 
		 // ajouter le type d'emission 
		 nbTypeEm.add(type + " " + emission.getJour() + " " + emission.getHeureDebut(), 1);
		 
		 // ajouter la categorie d'emission
		 String nomChaine = trouverNomEmission(emission.getIdChaine());
		 nbEmCat.add(nomChaine + " - " + emission.getCategorie(), 1);
		 
//		 System.out.println(type + " Acteur : ");
//		 for(int i=0; i<listActeur.size(); i++) {
//			 
//			 System.out.println("\t" + listActeur.get(i));
//		 }
		// System.out.println("Realisateur : ");
		// for(int i=0; i<listReal.size(); i++) {
		// System.out.println("\t" + listReal.get(i));
		// }

		return emission;

	}
	
	

	public  Personne constuireActeur(XMLStreamReader xmlsr) throws XMLStreamException {
		xmlsr.next();
		String[] nomComplet = xmlsr.getText().split(" ");
		String nom = null;
		String surnom = null;

		if (nomComplet.length > 2)
			surnom = nomComplet[2];
		if (nomComplet.length > 1)
			nom = nomComplet[1];
		return new Acteur(nomComplet[0], nom, surnom);

	}

	public  Personne constuireRealisateur(XMLStreamReader xmlsr) throws XMLStreamException {
		xmlsr.next();
		String[] nomComplet = xmlsr.getText().split(" ");
		String nom = null;

		if (nomComplet.length > 1)
			nom = nomComplet[1];
		return new Realisateur(nomComplet[0], nom);

	}

	public  int ignoreEspace(XMLStreamReader xmlsr) throws XMLStreamException {
		int evenType = xmlsr.next();

		while (xmlsr.hasNext()) {
			if (evenType != XMLEvent.SPACE)
				break;
			evenType = xmlsr.next();
		}

		return evenType;
	}

	public  List<Chaine> getListChaine() {
		return new ArrayList<Chaine>(listCh);
	}

	public  void afficherListChaine() {
		for (Chaine c : listCh) {
			System.out.println(c.toString());
		}
	}

	public  void addEmission(Map<String, List<Emission>> map, String id, Emission em) {
		List<Emission> list;
		if (map.containsKey(id)) {
			list = map.get(id);
			list.add(em);
			map.put(id, new ArrayList<Emission>(list));
		} else {
			list = new ArrayList<Emission>();
			list.add(em);
			map.put(id, list);
		}
	}

	public  List<Emission> listEmissionChaine(String idChaine) {
		return chaineEm.get(idChaine);
	}

	public  void afficherMap(Map<String, List<Emission>> map) {
		Iterator<Entry<String, List<Emission>>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, List<Emission>> e = it.next();
			//System.out.print(e.getKey() + "\n\t\n");
			//System.out.println(e.getValue().toString());
			
			System.out.println(e.getKey() + "\t");
			for(Emission em : e.getValue()) {
				System.out.print(em.getType() + " ");
			}
			System.out.println("\n");
		}
	}
	

	public  List<String> jourDemission() {
		Set<String> set = new TreeSet<>();
		Iterator<Entry<String, List<Emission>>> it = chaineEm.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, List<Emission>> e = it.next();
			// System.out.print(e.getKey() + "\n\t\n");
			// System.out.println(e.getValue().toString());
			String jour = null;
			for (Emission em : e.getValue()) {
				jour = em.getJour();
				set.add(jour);
			}
		}
		return new ArrayList<String>(set);
	}

	public  List<Emission> programmeDuJour(String chaine, String jour) {
		// TODO matcher jour avec le bon format jj/mois/annee

		List<Emission> list = new ArrayList<>();
		String id = trouverIdChaine(chaine);
		System.out.println(id);
		List<Emission> tmp = listEmissionChaine(id);
		// trouver les emission du jour passé en parametre
		for (Emission e : tmp) {
			String jourEm = e.getJour();
			if (jourEm.equals(jour)) {
				list.add(e);
			}
		}
		return list;
	}

	public  String trouverIdChaine(String nom) {
		// TODO
		// cnvertir le nom minuscule pour faciliter la recherche à l'utilisateur
		System.out.println(nom);
		String id = null;
		if (chaineId.containsKey(nom)) {
			id = chaineId.get(nom);
		} else {
			System.out.println("il n'est pas là!");
			// declancher une exception
		}
		return id;
	}

	public  String ficheEmission(String emission) {
		return emission.toString();
	}
	
	//********* pour tester
	public  void afficherHeureEmission() {
		Set<String> list = new TreeSet<>();
		Iterator<Entry<String, List<Emission>>> it = chaineEm.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, List<Emission>> e = it.next();
			String heure = null;
			for (Emission em : e.getValue()) {
				heure = em.getHeureDebut();
				//System.out.println(heure);
				list.add(heure);
			}
		}
		for(String s : list)
			System.out.println(s);
	}
	
	public  List<Emission> EmissionProgramme(String jour, String heure){
		List<Emission> list = new ArrayList<>();
		Iterator<Entry<String, List<Emission>>> it = chaineEm.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, List<Emission>> e = it.next();
			String h = null;
			String j = null;
			for (Emission em : e.getValue()) {
				h = em.getHeureDebut();
				j = em.getJour();
				if(j.equals(jour) && h.equals(heure))
					list.add(em);
			}
		}
		return list;
	}
	
	public  List<Emission> listFilmActReal(String nom, String prenom){
		String key = nom + " " + prenom;
		//Si la personne demande est un acteur
		if(actFilm.containsKey(key))
			return actFilm.get(key);
		//si la personne est un réalisateur
		if(realFilm.containsKey(key))
			return actFilm.get(key);
		System.out.println(key + " n'a participé en aucun film de la base");
		return null;
	}
	
	public  void compterFilmActeur(MultiSet<String> ms) {
		List<String> list = ms.elements();
		
		class MultiSetComparator implements Comparator<String> {
			MultiSet<String> ms;
			
			public MultiSetComparator(MultiSet<String> ms) {
				this.ms = ms;
			}
			
			public int compare(String o1, String o2) {
				return Integer.compare(ms.count(o2), ms.count(o1));
			}
		}
		
		Comparator<String> c = new MultiSetComparator(ms);
		
		list.sort(c);
		/*affiche les 20 1er elements*/
		for(int i=0; i<21; i++){
			System.out.println(list.get(i) + " : " + ms.count(list.get(i)));
		}
		//System.out.println(list.size());
	}
	
	private  void afficheListEmission(List<Emission> list) {
		if(list == null ||  list.size() == 0) {
			System.out.println("Exception, pas d'emission");
			return ;
		}
		for(Emission em : list) {
			System.out.println(em.toString());
		}
	}
	
	//trouver l'id d'une chaine à partir de son nom
	private  String trouverNomEmission(String id) {
		Iterator<Entry<String, String>> it = chaineId.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> e = it.next();
			if(e.getValue().equals(id)) {
				return e.getKey();
			}
		}
		return null;
	}
	
	private  void ajouterDateFilm(Map<String, Set<Integer>> map, String chaine, String date) {
		if(date != null) {
			Set<Integer> list;
			if(map.containsKey(chaine)) {
				list = map.get(chaine);
				list.add(new Integer(date));
				map.put(chaine, list);
			}
			else {
				list = new TreeSet<>();
				list.add(new Integer(date));
				map.put(chaine, list);
			}
		}
	}
		
	

}


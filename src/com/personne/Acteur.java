package com.personne;

public class Acteur extends Personne{
	private String surNom;

	public Acteur(String nom, String prenom, String surNom) {
		super(nom, prenom);
		this.surNom = surNom;
	}

	@Override
	public String toString() {
		return super.toString() +  " " + surNom ;
	}
	
	

}

package com.personne;

public abstract class Personne {
	/**
	 * 
	 */
	private String nom;
	/**
	 * 
	 */
	private String prenom;
	
	/**
	 * 
	 * @param nom
	 * @param prenom
	 */
	public Personne(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	/**
	 * 
	 */
	public Personne() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * 
	 * @return
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return  nom + " " + prenom ;
	}
	
	
}

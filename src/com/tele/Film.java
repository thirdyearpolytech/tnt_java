package com.tele;

import java.util.ArrayList;
import java.util.List;

import com.personne.Personne;


public class Film extends Emission{
	
	private List<Personne> listActeur;
	private List<Personne> listRealisateur;

	public Film(String type, String debut, String fin, int duree, String titre, String sousTitre, String desc, String continent,
			String categorie, String idChaine, String date) {
		super(type, debut, fin, duree, titre, sousTitre, desc, continent, categorie, idChaine, date);
		
		listActeur = new ArrayList<>();
		listRealisateur = new ArrayList<>();
	}
	
	public boolean addActeur(Personne p) {
		return listActeur.add(p);
	}
	
	public boolean addAllActeur(List<Personne> list) {
		return listActeur.addAll(list);
	}
	
	public boolean addRealisateur(Personne p) {
		return listRealisateur.add(p);
	}
	
	boolean addAllRealisateur(List<Personne> list) {
		return listRealisateur.addAll(list);
	}
	
	@Override
	public String toString() {
		String s = super.toString();
		if(listActeur != null || listActeur.size() != 0) {
			s += "Acteurs : \n" ;
			for(Personne p : listActeur) {
				s += p.getNom() + " " + p.getPrenom() + "\n";
			}
		}
		return s;
	}

}

package com.tele;

import java.util.ArrayList;
import java.util.List;
/**
 * Cette représente une chaine de télé
 * @author abdoulaye
 *
 */
public class Chaine {
	/**
	 * nom de la chaine
	 */
	private String nom;
	/**
	 * l'id de la chaine
	 */
	private final String id;
	/**
	 * le lien de l'icone de la chaine
	 */
	private String icon;
	
	/**
	 * constructeur de la chaine
	 * @param nom
	 * @param id
	 * @param icon
	 */
	public Chaine(String nom, String id, String icon) {
		super();
		this.nom = nom;
		this.id = id;
		this.icon = icon;
		//listE = new ArrayList<Emission>();
	}
	/**
	 * renvoie le nom de la chaine
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * renvoie l'id de la chaine
	 * @return
	 */
	public String getId() {
		return id;
	}
	/**
	 * renvoie le lien de l'icone de la chaine
	 * @return
	 */
	public String getIcon() {
		return icon;
	}
	
	@Override
	public String toString() {
		return  this.nom ;
	}
	
	
}

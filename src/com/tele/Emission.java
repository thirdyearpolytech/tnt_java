package com.tele;

import java.util.ArrayList;
import java.util.List;

import com.personne.Personne;

/**
 * Cette représente une émission d'une chaine de télé
 * @author abdoulaye
 *
 */
public class Emission {
	
	private List<Personne> listActeur;
	private List<Personne> listRealisateur;
	/**
	 * la date de début
	 */
	private String debut;
	/**
	 * la date de fin
	 */
	private String fin;
	/**
	 * le titre de l'émission
	 */
	private String titre;
	private String sousTitre;
	private String desc;
	private String continent;
	private String categorie;
	private String idChaine;
	private String date;
	private int duree; // en minute
	private String type;//pour tester
	private String jour;
	private String heureDebut;
	
	private List<Personne> listP;
	
	// private Image icon (à revoir)
	/**
	 * Constructeur de l'emission
	 * @param debut
	 * @param fin
	 * @param duree
	 * @param titre
	 * @param sousTitre
	 * @param desc
	 * @param continent
	 * @param categorie
	 * @param idChaine
	 * @param date
	 */
	public Emission(String debut, String fin, int duree, String titre, String sousTitre, String desc, String continent, String categorie,
			String idChaine, String date) {

		super();
		listActeur = new ArrayList<>();
		listRealisateur = new ArrayList<>();
		this.debut = debut;
		this.fin = fin;
		this.duree = duree;
		this.titre = titre;
		this.sousTitre = sousTitre;
		this.desc = desc;
		this.continent = continent;
		this.categorie = categorie;
		this.idChaine = idChaine;
		this.date = date;
		
		parserDate();
		
		listP = new ArrayList<>();
	}
	
	/**
	 * constructeur de l'emission avec son type
	 * @param type
	 * @param debut
	 * @param fin
	 * @param duree
	 * @param titre
	 * @param sousTitre
	 * @param desc
	 * @param continent
	 * @param categorie
	 * @param idChaine
	 * @param date
	 */
	public Emission(String type, String debut, String fin, int duree, String titre, String sousTitre, String desc, String continent, String categorie,
			String idChaine, String date) {

		
		super();
		listActeur = new ArrayList<>();
		listRealisateur = new ArrayList<>();
		this.type = type;
		this.debut = debut;
		this.fin = fin;
		this.duree = duree;
		this.titre = titre;
		this.sousTitre = sousTitre;
		this.desc = desc;
		this.continent = continent;
		this.categorie = categorie;
		this.idChaine = idChaine;
		this.date = date;
		
		parserDate();
	}
	
	boolean addPersonne(Personne p) {
		return listP.add(p);
	}
	
	boolean addAllPersonne(List<Personne> list) {
		return listP.addAll(list);
	}

	public String getDebut() {
		return debut;
	}

	public String getFin() {
		return fin;
	}

	public int getDuree() {
		return duree;
	}

	public String getTitre() {
		return titre;
	}

	public String getDesc() {
		return desc;
	}

	public String getContinent() {
		return continent;
	}

	public String getCategorie() {
		return categorie;
	}

	public String getIdChaine() {
		return idChaine;
	}

	public String getDate() {
		return date;
	}
	
	public String getJour() {
		return jour;
	}
	
	public String getHeureDebut() {
		return heureDebut;
	}
	
	public String getType() {
		return type;
	}
	/**
	 * récuperer la date et l'heure de l'emission
	 */
	private void parserDate() {
		//reécupérer la dat
		String y = debut.substring(0, 4);
		String m = debut.substring(4, 6);
		String d = debut.substring(6, 8);
		StringBuilder str = new StringBuilder();
		str.append(d); str.append("/"); str.append(m);  
		str.append("/"); str.append(y);
		jour = str.toString();
		//récuperer l'heure
		String h = debut.substring(8, 10);
		String min = debut.substring(10, 12);
		str = new StringBuilder();
		str.append(h); str.append(":");
		str.append(min);
		heureDebut = str.toString();
		
	}
	
//	boolean addActeur(Personne p) {
//		return listActeur.add(p);
//	}
//	
//	public boolean addAllActeur(List<Personne> list) {
//		return listActeur.addAll(list);
//	}
//	
//	boolean addRealisateur(Personne p) {
//		return listRealisateur.add(p);
//	}
//	
//	boolean addAllRealisateur(List<Personne> list) {
//		return listRealisateur.addAll(list);
//	}
	
//	@Override
//	public abstract String toString();
	
	@Override
	public String toString() {
		String s = this.type + "\n";
		s += "titre : " + this.titre + "\n";
		s += "sous-titre : " + this.sousTitre + "\n";
		s += "année : " + this.date + "\n";
		s += "categorie : " + this.categorie + "\n";
		s += "jour de diffusion : " + this.jour + " à " + this.heureDebut + "\n";
		s += "description : " + this.desc + "\n";
		
		return s;
	}

	public void add(List<Personne> listActeur2) {
		// TODO Auto-generated method stub
		
	}


}

